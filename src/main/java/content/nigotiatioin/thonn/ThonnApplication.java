package content.nigotiatioin.thonn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThonnApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThonnApplication.class, args);
    }

}
