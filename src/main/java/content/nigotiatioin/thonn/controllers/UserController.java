package content.nigotiatioin.thonn.controllers;

import content.nigotiatioin.thonn.models.Users;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users")
public class UserController {

    private static List<Users> allUsers = new ArrayList<>();

    static {
        allUsers.add(new Users(1,"Dara_01","Male","dara_01@gmail.com","PP1"));
        allUsers.add(new Users(2,"Dara_02","Male","dara_02@gmail.com","PP2"));
        allUsers.add(new Users(3,"Dara_03","Male","dara_03@gmail.com","PP3"));
        allUsers.add(new Users(4,"Dara_04","Male","dara_04@gmail.com","PP4"));
        allUsers.add(new Users(5,"Dara_05","Male","dara_05@gmail.com","PP5"));
        System.out.println(allUsers.size());
    }

//    @GetMapping
    @RequestMapping(method = RequestMethod.GET, produces = {"application/json", "application/xml"})
    public ResponseEntity<Map<String, Object>> allUser(){
        Map<String, Object> response =new HashMap<>();
        response.put("data", allUsers);
        response.put("msg", "success");
        response.put("code", "0000");
        return new ResponseEntity(response, HttpStatus.OK);
    }


}
